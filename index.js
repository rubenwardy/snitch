const Discord = require("discord.js");
const id = process.env.WEBHOOK_ID;
const token = process.env.WEBHOOK_TOKEN;
const hook = new Discord.WebhookClient(id, token);

function reportBug(source, error) {
	const tilda = "```";
	const date = new Date().toLocaleString("en-gb");

	if (error.length > 1800) {
		error = error.substring(error.length - 1800);
	}

	hook.send(`!!\nCrash reported at ${date}\n**Source:** ${source}`);
	hook.send(`${tilda}\n${error}\n${tilda}`);
}


const express = require("express");
const app = express();
const port = 9911;
const textBody = require("body");

app.post("/", (req, res) => {
	textBody(req, res, function (err, body) {
		if (err) {
			res.statusCode = 500;
			return res.end("ERROR");
		}

		const source = req.query.source;
		const message = body.trim();
		reportBug(source, message);
		res.end("OK")
	})
;});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));