# Snitch

A service to receive crash reports from other processes, and to keep secrets
in one location

    curl -d "Error message" -X POST http://localhost:9911/?source=service.name
    
You should deploy using pm2:

```yml
apps:
  - script: ./index.js
    name: snitch
    watch: true
    env:
      WEBHOOK_ID: "1234"
      WEBHOOK_TOKEN: TOKEN
```